from datetime import datetime
from .models import Customer

def send_birthday_emails():
    today = datetime.now().date()
    customers = Customer.objects.filter(birthday__day=today.day, birthday__month=today.month)
    for customer in customers:
        # Birthday email to customer
        print(f"Sending birthday email to {customer.name} at {customer.email}")
        print("We can prepare an email template here and send it using smtplib.")
        print()