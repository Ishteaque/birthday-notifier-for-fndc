from django.core.management.base import BaseCommand
from customers.tasks import send_birthday_emails
import schedule
import time

class Command(BaseCommand):
    help = 'Runs scheduled tasks for sending birthday emails'

    def handle(self, *args, **options):
        # We can schedule the task to run daily at 12:00 AM or whenever we want
        schedule.every().day.at("00:00").do(send_birthday_emails)

        # Run the scheduler continuously
        while True:
            schedule.run_pending()
            time.sleep(1)