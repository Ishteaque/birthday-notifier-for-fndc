# Birthday Reminder System

The Birthday Reminder System is a Django project that Simulates automated birthday greetings email sending to customers. It utilizes Django Rest Framework (DRF) for building a RESTful API, task scheduling with the `schedule` library.

## Features

- RESTful API for registering customers and retrieving customer data.
- Automated task scheduling to send birthday emails to customers.
- Simulated email sending process for testing purposes.

## Installation

1. Clone the repository to the local machine:

```bash
git clone https://gitlab.com/Ishteaque/birthday-notifier-for-fndc.git
```

2. Navigate to the project directory: "cd birthday-notifier-for-fndc"

3. Create a virtual environment and activate it (Here, we can use 'AnacondaPrompt' and as there are distribution issues for different Django versions, so need to use Python>3.9, I have used "python 3.11"):
```bash
pip install virtualenv (if needed)
virtualenv venv
OR,
virtualenv venv -p E:\Path\to\Python3.11\python.exe

source venv/bin/activate  # For Unix/Linux
venv\Scripts\activate  # For Windows
```
4. Go inside the project ("cd birthday_notifier") & Install dependencies:
```bash
pip install -r requirements.txt
```
5. Run database migrations:
```bash
python manage.py migrate
```
6. Start the Django development server:
```bash
python manage.py runserver
```
At this point you can add new customers. Go to the url: 
http://127.0.0.1:8000/customer/register


7. Run the scheduler for sending birthday emails (simulation). Here you can use a new terminal or terminate the previous command and use the same terminal.
```bash
python manage.py birthday_scheduler
```
- The system checks everyday at 12:00 AM if we have any customers birthday today (birthday_scheduler.py). If yes, the scheduer performs the task "send_birthday_emails" specified in tasks.py.

### If you want:
- You can also update the scheduler from this file: 'birthday_notifier\customers\management\commands\birthday_scheduler.py'
- Like, Line 11: schedule.every().day.at("00:09").do(send_birthday_emails), Here, ("00:09") => 12:09 AM, ("15:09") => 03:09 PM and so on.
- Useful commands for modification in the DB:
```bash
python manage.py shell
from customers.models import Customer
import datetime
customer = Customer.objects.get(email='ishte@example.com')    
customer.birthday=datetime.date(1998, 4, 25)
customer.save()
``` 

### Other possible options:
- We could use celery with Redis
- We could use django.crontab (Linux)